'use strict';

var url = require('url');


var Default = require('./DefaultService');


module.exports.javaprojectsIdAmplifiedunittestsGET = function javaprojectsIdAmplifiedunittestsGET (req, res, next) {
  Default.javaprojectsIdAmplifiedunittestsGET(req.swagger.params, res, next);
};

module.exports.javaprojectsPOST = function javaprojectsPOST (req, res, next) {
  Default.javaprojectsPOST(req.swagger.params, res, next);
};
