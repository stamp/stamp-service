'use strict';

exports.javaprojectsIdAmplifiedunittestsGET = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{ \"id\" : \"CBA4C53B83C96FA1B0BBAFBE791781E7\"\n    \"amplified_tests\" : \"UEsDBBQDAAAAAF2IhkoAAAAAAAAAAAAAAAAKAAAAZHNwb3Qtb3V0L1BLAwQUAwAAAABZiIZKAAAAAAAAAAAAAAAAEgAAAGRzcG90LW91dC9leGFtcGxlL1BLAwQUAwAACADBiIZKcu3xeIwAAAAMAgAANwAAAGRzcG90LW91dC9leGFtcGxlLlRlc3RTdWl0ZUV4YW1wbGVfYnJhbmNoX2NvdmVyYWdlLmpzb26r5uJUSq1IzC3ISdULSS0uCS7NLEl1hQgoWSkApTmVSoDiFvHJaYYG5hAhoJiyY3FxalFJZn6eY0pKaoqSlZkORNwzr6C0BFUsJzUvvSQDyAXyanVgJpoATTSzpMhAExQDjUBONCPNQAtUA01RDLQEGWhsSEUTzYEmWpDvZ0wDjYEGmhhQbiAXENUCAFBLAwQUAwAACADBiIZKzCL5P6IAAADtAAAAPQAAAGRzcG90LW91dC9leGFtcGxlLlRlc3RTdWl0ZUV4YW1wbGVfYnJhbmNoX2NvdmVyYWdlX3JlcG9ydC50eHRNjjsOwjAQRHufYhq6FEFBCEWiAVFQgUIu4IQlWcnYwV6T65MfUrZbzTzNU8f5UFzut6LE8qqT17ZucXZf8rohPMhQLc7n6mpZWBvUS5TjkCVZtlFlS57AARmi5U8kdFpasIW0BOe5YTtwQkEQIguNBPS7M/ziWgs7C08hGgnoeSD3sNRP/TBVq9npPwxXiWZLT3DIsU3TJE1XFruRXpso9QNQSwMEFAMAAAgAwYiGSsOQWOmLAgAAkBEAACsAAABkc3BvdC1vdXQvZXhhbXBsZS9UZXN0U3VpdGVFeGFtcGxlQW1wbC5qYXZhzZfPjpswEMbveQprLwGJRdg4xFa0UnPYJ9jekUMIIUvINpC0UtV3r8F0Zzd/bVyqcggK8Td84/kxGUajN5G8iixF6Q+xfSvS2UheOiyKPEFJIaoKfU2r+uWQ1+mzWjCXH+jnCMnjy26f+ZtDmdd+s6q91mmPu3yJanmROG67Wh3dTfwulvyOnlCZfj/9wXFn7xq4ybyq0n3ti/b0/O0gisoZL8eeVPvJWuzntfMgFsnywUOh20X4NdK2Glpb3Yij8AtRZv5Lvc/LDFVSoyz1zKfykONUfpGWWb12XBc9Iuz2yI3+n7lh0iOXqXUud2wmF5HqY5UNbXVx0SruYZUPbXV1bjVdZet881pspefJbc9OnW/T3aGWNnAgD/dyt4mTFY6sM8nLGh2TmMqVj5iGhDM6DbBupo3SQ84HpXvrOVKnWKoC4yfqg9brpO7saupyGb6ffFMgtIthO+OYS1Uj7or3+bZNutoUnMT10FiMB+71euSE0hINrMm53yUBL97ixQgOQxJMAgO8eIMXKDXxIhZ4EcDrFBTYvThm6AkK9Fne2DbDBKIqTP7dX6geMlTai+zbpgkyuGlJIeOM4xAzA2Iw9UCnhwu1wIVexwV27hwXkKtkzXiBsB6SDLjDjSV6fEylHWbPx5/Sq3YRTRglJOAmtW/aBSg1u0VkUf7oevlhY+J4eqX8kSq/YbuAsG27GHjc00OASUs4sB9YOwaImkgICSkJ6CQygIC0IwlINSlgFhSwGxTA3pxjAHqVsxkGEFdxMPAsrccBbzyF+K9xoHoB54xGhIcTEw7aZgBSTQ7w+2wKM7s5ETjwToLcGVdJpD+vwja3A6uSA1XgQWFl2l0gdovVEO89krDfUEsDBBQDAAAIAMWIhkrQ7aiYcQAAAJYAAAAcAAAAZHNwb3Qtb3V0L3Rlc3QtcHJvamVjdHMuanNvbqvmUlBQSs5JLC4OycxNLVayUojmUgACoDgYKKWV5uQElibmZKZlpqb4JeamApUopVYk5hbkpOqFpBaXBJdmlqS6QgSUdGDaSoCmeeb5ggw0NDAwNgaL1wLJWJASpYKi/KzU5BKYeSVAc3ShYsVKXLUAUEsBAj8DFAMAAAAAXYiGSgAAAAAAAAAAAAAAAAoAAAAAAAAAAAAQgO1BAAAAAGRzcG90LW91dC9QSwECPwMUAwAAAABZiIZKAAAAAAAAAAAAAAAAEgAAAAAAAAAAABCA7UEoAAAAZHNwb3Qtb3V0L2V4YW1wbGUvUEsBAj8DFAMAAAgAwYiGSnLt8XiMAAAADAIAADcAAAAAAAAAAAAggKSBWAAAAGRzcG90LW91dC9leGFtcGxlLlRlc3RTdWl0ZUV4YW1wbGVfYnJhbmNoX2NvdmVyYWdlLmpzb25QSwECPwMUAwAACADBiIZKzCL5P6IAAADtAAAAPQAAAAAAAAAAACCApIE5AQAAZHNwb3Qtb3V0L2V4YW1wbGUuVGVzdFN1aXRlRXhhbXBsZV9icmFuY2hfY292ZXJhZ2VfcmVwb3J0LnR4dFBLAQI/AxQDAAAIAMGIhkrDkFjpiwIAAJARAAArAAAAAAAAAAAAIICkgTYCAABkc3BvdC1vdXQvZXhhbXBsZS9UZXN0U3VpdGVFeGFtcGxlQW1wbC5qYXZhUEsBAj8DFAMAAAgAxYiGStDtqJhxAAAAlgAAABwAAAAAAAAAAAAggKSBCgUAAGRzcG90LW91dC90ZXN0LXByb2plY3RzLmpzb25QSwUGAAAAAAYABgDrAQAAtQUAAAAA\"\n}\n";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.javaprojectsPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * project (Project)
  **/
    var examples = {};
  examples['application/json'] = "{\"id\" : \"E85C24916AB75382FE1BC9235406B400\"}\n";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

