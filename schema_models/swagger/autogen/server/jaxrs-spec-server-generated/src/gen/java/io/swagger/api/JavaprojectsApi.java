package io.swagger.api;

import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse201;
import io.swagger.model.InlineResponse409;
import io.swagger.model.Project;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.util.List;

@Path("/javaprojects")

@Api(description = "the javaprojects API")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2017-04-22T13:31:33.275Z")

public class JavaprojectsApi  {

    @GET
    @Path("/{id}/amplifiedunittests")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "", notes = "retrieves amplified unit tests from the input project", response = InlineResponse200.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Amplified unit tests", response = InlineResponse200.class),
        @ApiResponse(code = 404, message = "Project not found", response = InlineResponse200.class) })
    public Response javaprojectsIdAmplifiedunittestsGet(@PathParam("id") String id) {
    	return Response.ok().entity("magic!").build();
    }

    @POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "upload java project", notes = "upload a java project (a developer working copy) containing unit tests. Java project shall be a Maven project", response = InlineResponse201.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Project uploaded", response = InlineResponse201.class),
        @ApiResponse(code = 409, message = "Duplicate java project found", response = InlineResponse201.class) })
    public Response javaprojectsPost(Project project) {
    	return Response.ok().entity("magic!").build();
    }
}

