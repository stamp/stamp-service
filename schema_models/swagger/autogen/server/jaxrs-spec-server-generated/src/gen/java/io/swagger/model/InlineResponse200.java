package io.swagger.model;




import io.swagger.annotations.*;
import java.util.Objects;


public class InlineResponse200   {
  
  private String id = null;
  private String amplifiedTests = null;

  /**
   **/
  public InlineResponse200 id(String id) {
    this.id = id;
    return this;
  }

  
  @ApiModelProperty(value = "")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   **/
  public InlineResponse200 amplifiedTests(String amplifiedTests) {
    this.amplifiedTests = amplifiedTests;
    return this;
  }

  
  @ApiModelProperty(value = "")
  public String getAmplifiedTests() {
    return amplifiedTests;
  }
  public void setAmplifiedTests(String amplifiedTests) {
    this.amplifiedTests = amplifiedTests;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse200 inlineResponse200 = (InlineResponse200) o;
    return Objects.equals(id, inlineResponse200.id) &&
        Objects.equals(amplifiedTests, inlineResponse200.amplifiedTests);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, amplifiedTests);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse200 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    amplifiedTests: ").append(toIndentedString(amplifiedTests)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
