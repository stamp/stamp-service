package io.swagger.model;




import io.swagger.annotations.*;
import java.util.Objects;


public class Project   {
  
  private String sources = null;

  /**
   * zipped and base64-encoded sources (maven project root folder)
   **/
  public Project sources(String sources) {
    this.sources = sources;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "zipped and base64-encoded sources (maven project root folder)")
  public String getSources() {
    return sources;
  }
  public void setSources(String sources) {
    this.sources = sources;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Project project = (Project) o;
    return Objects.equals(sources, project.sources);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sources);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Project {\n");
    
    sb.append("    sources: ").append(toIndentedString(sources)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
