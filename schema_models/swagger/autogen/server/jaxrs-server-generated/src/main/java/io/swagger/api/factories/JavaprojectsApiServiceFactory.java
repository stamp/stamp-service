package io.swagger.api.factories;

import io.swagger.api.JavaprojectsApiService;
import io.swagger.api.impl.JavaprojectsApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-04-22T13:31:22.070Z")
public class JavaprojectsApiServiceFactory {
    private final static JavaprojectsApiService service = new JavaprojectsApiServiceImpl();

    public static JavaprojectsApiService getJavaprojectsApi() {
        return service;
    }
}
