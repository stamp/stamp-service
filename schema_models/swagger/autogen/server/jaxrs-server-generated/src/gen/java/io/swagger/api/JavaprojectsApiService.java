package io.swagger.api;

import io.swagger.api.*;
import io.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse201;
import io.swagger.model.InlineResponse409;
import io.swagger.model.Project;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-04-22T13:31:22.070Z")
public abstract class JavaprojectsApiService {
    public abstract Response javaprojectsIdAmplifiedunittestsGet(String id,SecurityContext securityContext) throws NotFoundException;
    public abstract Response javaprojectsPost(Project project,SecurityContext securityContext) throws NotFoundException;
}
