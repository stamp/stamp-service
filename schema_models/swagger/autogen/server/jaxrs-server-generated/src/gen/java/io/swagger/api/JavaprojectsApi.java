package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.JavaprojectsApiService;
import io.swagger.api.factories.JavaprojectsApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse201;
import io.swagger.model.InlineResponse409;
import io.swagger.model.Project;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/javaprojects")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@io.swagger.annotations.Api(description = "the javaprojects API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-04-22T13:31:22.070Z")
public class JavaprojectsApi  {
   private final JavaprojectsApiService delegate = JavaprojectsApiServiceFactory.getJavaprojectsApi();

    @GET
    @Path("/{id}/amplifiedunittests")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "retrieves amplified unit tests from the input project", response = InlineResponse200.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "Amplified unit tests", response = InlineResponse200.class),
        
        @io.swagger.annotations.ApiResponse(code = 404, message = "Project not found", response = InlineResponse200.class) })
    public Response javaprojectsIdAmplifiedunittestsGet(@ApiParam(value = "project identifier",required=true) @PathParam("id") String id
,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.javaprojectsIdAmplifiedunittestsGet(id,securityContext);
    }
    @POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "upload java project", notes = "upload a java project (a developer working copy) containing unit tests. Java project shall be a Maven project", response = InlineResponse201.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 201, message = "Project uploaded", response = InlineResponse201.class),
        
        @io.swagger.annotations.ApiResponse(code = 409, message = "Duplicate java project found", response = InlineResponse201.class) })
    public Response javaprojectsPost(@ApiParam(value = "java project containing unit tests to amplify" ,required=true) Project project
,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.javaprojectsPost(project,securityContext);
    }
}
