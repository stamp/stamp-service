package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.InlineResponse200;
import io.swagger.client.model.InlineResponse201;
import io.swagger.client.model.InlineResponse409;
import io.swagger.client.model.Project;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for DefaultApi
 */
public class DefaultApiTest {

    private final DefaultApi api = new DefaultApi();

    
    /**
     * 
     *
     * retrieves amplified unit tests from the input project
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void javaprojectsIdAmplifiedunittestsGetTest() throws ApiException {
        String id = null;
        // InlineResponse200 response = api.javaprojectsIdAmplifiedunittestsGet(id);

        // TODO: test validations
    }
    
    /**
     * upload java project
     *
     * upload a java project (a developer working copy) containing unit tests. Java project shall be a Maven project
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void javaprojectsPostTest() throws ApiException {
        Project project = null;
        // InlineResponse201 response = api.javaprojectsPost(project);

        // TODO: test validations
    }
    
}
