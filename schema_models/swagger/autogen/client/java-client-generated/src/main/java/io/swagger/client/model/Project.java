package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Project
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-04-22T13:30:31.562Z")
public class Project {
  @SerializedName("sources")
  private String sources = null;

  public Project sources(String sources) {
    this.sources = sources;
    return this;
  }

   /**
   * zipped and base64-encoded sources (maven project root folder)
   * @return sources
  **/
  @ApiModelProperty(required = true, value = "zipped and base64-encoded sources (maven project root folder)")
  public String getSources() {
    return sources;
  }

  public void setSources(String sources) {
    this.sources = sources;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Project project = (Project) o;
    return Objects.equals(this.sources, project.sources);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sources);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Project {\n");
    
    sb.append("    sources: ").append(toIndentedString(sources)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

