package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * InlineResponse200
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-04-22T13:30:31.562Z")
public class InlineResponse200 {
  @SerializedName("id")
  private String id = null;

  @SerializedName("amplified_tests")
  private String amplifiedTests = null;

  public InlineResponse200 id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public InlineResponse200 amplifiedTests(String amplifiedTests) {
    this.amplifiedTests = amplifiedTests;
    return this;
  }

   /**
   * Get amplifiedTests
   * @return amplifiedTests
  **/
  @ApiModelProperty(value = "")
  public String getAmplifiedTests() {
    return amplifiedTests;
  }

  public void setAmplifiedTests(String amplifiedTests) {
    this.amplifiedTests = amplifiedTests;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse200 inlineResponse200 = (InlineResponse200) o;
    return Objects.equals(this.id, inlineResponse200.id) &&
        Objects.equals(this.amplifiedTests, inlineResponse200.amplifiedTests);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, amplifiedTests);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse200 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    amplifiedTests: ").append(toIndentedString(amplifiedTests)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

