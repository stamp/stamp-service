# DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/stampers/stamp-api/0.1.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**javaprojectsIdAmplifiedunittestsGet**](DefaultApi.md#javaprojectsIdAmplifiedunittestsGet) | **GET** /javaprojects/{id}/amplifiedunittests | 
[**javaprojectsPost**](DefaultApi.md#javaprojectsPost) | **POST** /javaprojects | upload java project


<a name="javaprojectsIdAmplifiedunittestsGet"></a>
# **javaprojectsIdAmplifiedunittestsGet**
> InlineResponse200 javaprojectsIdAmplifiedunittestsGet(id)



retrieves amplified unit tests from the input project

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | project identifier
try {
    InlineResponse200 result = apiInstance.javaprojectsIdAmplifiedunittestsGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#javaprojectsIdAmplifiedunittestsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| project identifier |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="javaprojectsPost"></a>
# **javaprojectsPost**
> InlineResponse201 javaprojectsPost(project)

upload java project

upload a java project (a developer working copy) containing unit tests. Java project shall be a Maven project

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Project project = new Project(); // Project | java project containing unit tests to amplify
try {
    InlineResponse201 result = apiInstance.javaprojectsPost(project);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#javaprojectsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | [**Project**](Project.md)| java project containing unit tests to amplify |

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

