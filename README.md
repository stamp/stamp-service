STAMP Services
==============
STAMP Services expose STAMP features through a set of microservices.

## License
STAMP services are freely available under the [LGPL license](LICENSE).